@Environment:Pruebas

@FeatureName:ComputersNoCommerceTests

Feature:
  Yo como comprador de la tienda
  Quiero poder realizar la visualización de computadores que venden en un sitio web
  Para no tener que ir a la tienda física

  #--------------------------Scenario HU00106
  #--------------------------Caso de prueba 1

  Scenario: Ver informacion de los productos de Desktops en lista
    Given que estoy en la pagina de computers
    When ingreso a la cateogria Desktops
    And los posiciono en lista
    Then mostrara la informacion de los productos

  #--------------------------Caso de prueba 2

  Scenario: Ver informacion de los productos de Notebooks en lista
    Given que estoy en la pagina de computers
    When  ingreso a la cateogria Notebooks
    And los posiciono en lista los Noteboooks
    Then mostrara la informacion de los productos de esta categoria

