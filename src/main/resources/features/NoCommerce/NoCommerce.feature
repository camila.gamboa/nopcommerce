@Environment:Pruebas

@FeatureName:NoCommerceTests

Feature:
  Yo como comprador de la tienda
  Quiero poder loguearme a la plataforma
  Para realizar comprar a través de internet.

#--------------Scenario HU00202
  Scenario: Registrarme en la plataforma
    Given que estoy en la pagina nopCommerce para registrarme
    When ingreso a la sección de registro
    And diligencio todos los campos
    Then me registro exitosamente

    #-------------Scenario HU00301, HU00302
  Scenario: Deseo loguearme exitosamente
    Given que estoy en la página nopCommerce para loguearme
    When igreso a Log in
    And ingreso email y password válido
    Then igresare existosamente a la plataforma y mostrará el mensaje Welcome to our store

    #--------------Scenario HU00304
  Scenario: Intento de logueo inválido
    Given que estoy en la página nopCommerce para loguearme
    When igreso a Log in
    And ingreso email y password invalido
    Then al presionar ingresar el sistema me muestra un mensaje de error

    #---------------Scenario HU00306
  Scenario: Deseo poder salir de la plataforma después de haberme logueado
    Given que estoy logueado en la página nopCommerce
    When le doy click en log out para salir de la plataforma
    Then regresaré a la pagina de inicio y estará disponible el botón de Log in
