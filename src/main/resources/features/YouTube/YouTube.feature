@Environment:Pruebas
@FeatureName:YouTube

Feature: Play a video in YouTube

  #ESCENARIO 1
  @Busqueda
  Scenario Outline: Search a <song> in YouTube
    Given that I am in the page of YouTube
    When I search the video <song>
    Then will appear the list of related videos
    Examples:
      | song     |
      | macarena |
      | lalala   |