package co.com.client.desktopProject.test.helpers;

public class Dictionary {
    public static class Operacion{
        public static final String SUMA = "suma";
        public static final String RESTA = "resta";
        public static final String MULTIPLICACION = "multiplicacion";
        public static final String DIVISION = "division";
    }
    public static class Numero{
        public static final String UNO = "1";
        public static final String DOS = "2";
        public static final String TRES = "3";
        public static final String CUATRO = "4";
        public static final String CINCO = "5";
        public static final String SEIS = "6";
        public static final String SIETE = "7";
        public static final String OCHO = "8";
        public static final String NUEVE = "9";
        public static final String CERO = "0";
    }
}
