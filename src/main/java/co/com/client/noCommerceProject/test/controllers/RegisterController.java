package co.com.client.noCommerceProject.test.controllers;

import co.com.client.noCommerceProject.test.page.PageLogIn;
import co.com.client.noCommerceProject.test.page.PageRegister;
import co.com.client.noCommerceProject.test.stepdefinition.StepsDefinition;
import co.com.sofka.test.automationtools.selenium.Browser;
import co.com.sofka.test.evidence.reports.Assert;
import co.com.sofka.test.evidence.reports.Report;
import co.com.sofka.test.exceptions.WebActionsException;
import com.github.javafaker.Company;
import com.github.javafaker.Faker;
import java.util.Locale;
import java.util.Random;

public class RegisterController {

    private final String mensajeExitoso = "Se ha realizado exitosamente la accion.";
    private final String mensajeNoExitoso = "Ocurrió un error realizando la accion";

    Faker faker = Faker.instance(new Locale("en", "US"), new Random());

    public void startApp(String url, String feature) {
        final String accion = "Iniciación de plataforma";
        Browser browser = new Browser();
        browser.setBrowser(Browser.Browsers.CHROME);
        browser.setMaximized(true);
        browser.setIncognito(true);
        browser.setAutoDriverDownload(true);
        try {
            StepsDefinition.internalActionWeb.startWebApp(browser, url, feature);
            Report.reportScreenshot(StepsDefinition.internalActionWeb);
            Report.reportSuccess(String.format(mensajeExitoso, accion));
        } catch (WebActionsException e) {
            Report.reportScreenshot(StepsDefinition.internalActionWeb);
            Report.reportFailure(String.format(mensajeNoExitoso, accion),e);
        }
    }

    public void goaAndVerifyRegister() throws WebActionsException {
        final String accion = "Ingreso a Register";
        PageRegister pageRegister = new PageRegister();
        boolean respuesta = pageRegister.register(StepsDefinition.internalActionWeb);
        Assert.Hard.thatIsTrue(respuesta);
        Report.reportScreenshot(co.com.client.noCommerceProject.test.stepdefinition.StepsDefinition.internalActionWeb);
        Report.reportSuccess(String.format(mensajeExitoso,accion));
    }

    public void fillPersonalDetails() throws WebActionsException {
        String firstNameFill = faker.name().firstName();
        String lastNameFill = faker.name().lastName();
        String Email = faker.internet().safeEmailAddress();
        Company companyFill = faker.company();
        String passwordFill = faker.internet().password(6,20);
        String confirmPasswordFill = passwordFill;
        final String accion ="Rellenar formulario sección Your Personal Details";
        PageRegister pageRegister = new PageRegister();
        pageRegister.fixingRegister(firstNameFill, lastNameFill, Email, companyFill, passwordFill, confirmPasswordFill);

    }

    public void verifySuccessRegister() throws WebActionsException {
        PageRegister pageRegister = new PageRegister();
        boolean respuesta = pageRegister.registerSuccess(StepsDefinition.internalActionWeb);
        Assert.Soft.thatIsTrue(respuesta);
    }


}
