package co.com.client.noCommerceProject.test.controllers;

import co.com.client.noCommerceProject.test.page.PageLogIn;
import co.com.client.noCommerceProject.test.stepdefinition.StepsDefinition;
import co.com.sofka.test.automationtools.selenium.Browser;
import co.com.sofka.test.evidence.reports.Assert;
import co.com.sofka.test.evidence.reports.Report;
import co.com.sofka.test.exceptions.WebActionsException;
import com.github.javafaker.Faker;

import java.util.Locale;
import java.util.Random;

public class LogInController {

    private final String mensajeExitoso = "Se ha realizado exitosamente la accion.";
    private final String mensajeNoExitoso = "Ocurrió un error realizando la accion";

    Faker faker = Faker.instance(new Locale("en", "US"), new Random());


    public void startApp(String url, String feature) {
        final String accion = "Iniciación de plataforma";
        Browser browser = new Browser();
        browser.setBrowser(Browser.Browsers.CHROME);
        browser.setMaximized(true);
        browser.setIncognito(true);
        browser.setAutoDriverDownload(true);
        try {
            StepsDefinition.internalActionWeb.startWebApp(browser, url, feature);
            Report.reportScreenshot(StepsDefinition.internalActionWeb);
            Report.reportSuccess(String.format(mensajeExitoso, accion));
        } catch (WebActionsException e) {
            Report.reportScreenshot(StepsDefinition.internalActionWeb);
            Report.reportFailure(String.format(mensajeNoExitoso, accion), e);
        }
    }

    public void goAndVerifyLogIn() throws WebActionsException {
        final String accion = "Ingreso a Log in";
        PageLogIn pageLogIn = new PageLogIn();
        pageLogIn.logInPage(StepsDefinition.internalActionWeb);
    }

    public void fillLogIn(String emailCustomer, String passwordCustomer) throws WebActionsException {
        PageLogIn pageLogIn = new PageLogIn();
        pageLogIn.returningCustomer(emailCustomer, passwordCustomer);
    }

    public void verifySuccessLogIn() throws WebActionsException {
        PageLogIn pageLogIn = new PageLogIn();
        boolean respuesta = pageLogIn.verifyLogIn(StepsDefinition.internalActionWeb);
        Assert.Soft.thatIsTrue(respuesta);
    }

    public void logInInvalid()throws WebActionsException {
        String emailCustomer = faker.internet().safeEmailAddress();
        String passwordCustomer = faker.internet().password();
        PageLogIn pageLogIn = new PageLogIn();
        pageLogIn.returningCustomer(emailCustomer, passwordCustomer);
    }

    public void verifyMistakeMessage() throws WebActionsException {
        PageLogIn pageLogIn = new PageLogIn();
        boolean respuesta = pageLogIn.invalidLogInMessage(StepsDefinition.internalActionWeb);
        Assert.Soft.thatIsTrue(respuesta);
    }

    public void logOutOfPlatform() throws WebActionsException {
        PageLogIn pageLogIn = new PageLogIn();
        pageLogIn.logOutPlatform(StepsDefinition.internalActionWeb);
    }
}
