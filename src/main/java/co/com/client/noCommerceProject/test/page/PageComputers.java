package co.com.client.noCommerceProject.test.page;

import co.com.client.noCommerceProject.test.internalaction.InternalActionWeb;
import co.com.client.noCommerceProject.test.stepdefinition.StepsDefinition;
import co.com.sofka.test.evidence.reports.Assert;
import co.com.sofka.test.exceptions.WebActionsException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static co.com.client.noCommerceProject.test.stepdefinition.StepsDefinition.internalActionWeb;

public class PageComputers {

    @FindBy(xpath = "//ul[@class='top-menu notmobile']//a[@href='/computers']")
    private WebElement btnComputers;

    @FindBy(xpath = "//a[@href='/desktops' and @title='Show products in category Desktops']")
    private WebElement categoryDesktops;

    @FindBy(xpath = "//a[@href='/notebooks' and @title='Show products in category Notebooks']")
    private WebElement categoryNotebooks;

    @FindBy(xpath = "//a[@href='/software' and @title='Show products in category Software']")
    private WebElement categorySoftware;

    @FindBy(xpath = "//a[@class='viewmode-icon list']")
    private WebElement listForm;

    @FindBy(xpath = "//a[@class='viewmode-icon grid selected']")
    private WebElement squareForm;

    @FindBy(xpath = "//h2[@class='product-title']")
    private WebElement nameProduct;


    private static final int DEFAULT_TIMEOUT = 30;

    public PageComputers() {
        PageFactory.initElements(internalActionWeb.getDriver(), this);
    }

    public void categorysOfComputers() throws WebActionsException {
        internalActionWeb.click(btnComputers, DEFAULT_TIMEOUT, true);
    }

    public void btnCategoryDesktop() throws WebActionsException {
        internalActionWeb.click(categoryDesktops, DEFAULT_TIMEOUT, true);
    }

    public void btnCategoryNotebooks() throws WebActionsException {
        internalActionWeb.click(categoryNotebooks, DEFAULT_TIMEOUT, true);
    }

    public void btnCategorySoftware() throws WebActionsException {
        internalActionWeb.click(categorySoftware, DEFAULT_TIMEOUT, true);
    }

    public void setListForm() throws WebActionsException {
        internalActionWeb.click(listForm, DEFAULT_TIMEOUT, true);
    }

    public void setSquareForm() throws WebActionsException {
        internalActionWeb.click(squareForm, DEFAULT_TIMEOUT, true);
    }

    public boolean nameOfProducts(InternalActionWeb internalActionWeb) throws WebActionsException {
        StepsDefinition.internalActionWeb.isVisible(nameProduct, DEFAULT_TIMEOUT, true);
        return true;
    }

    public void nameOfProductOnSoftware() throws WebActionsException {
        Assert.Soft.thatIsEqual(nameProduct, "Adobe Photoshop CS4");
        Assert.Soft.thatIsEqual(nameProduct, "Sound Forge Pro 11 (recurring)");
        Assert.Soft.thatIsEqual(nameProduct, "Windows 8 Pro");
    }


}
