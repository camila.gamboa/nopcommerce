package co.com.client.noCommerceProject.test.page;

import co.com.client.noCommerceProject.test.internalaction.InternalActionWeb;
import co.com.sofka.test.exceptions.WebActionsException;
import com.github.javafaker.Company;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static co.com.client.noCommerceProject.test.stepdefinition.StepsDefinition.internalActionWeb;

public class PageRegister {

    @FindBy(xpath = "//a[@class='ico-register']")
    private WebElement btnRegister;

    @FindBy(className = "master-wrapper-content")
    private WebElement verifyRegisterPage;

    @FindBy(id = "gender-male")
    private WebElement btnGenderMale;

    @FindBy(id = "gender-female")
    private WebElement btnGenderFemale;

    @FindBy(id = "FirstName")
    private WebElement firstName;

    @FindBy(id = "LastName")
    private WebElement lastName;

    @FindBy(id = "Email")
    private WebElement email;

    @FindBy(id = "NumeroIdentidad")
    private WebElement numeroIdentidad;

    @FindBy(id = "Company")
    private WebElement company;

    @FindBy(id = "Password")
    private WebElement password;

    @FindBy(id = "ConfirmPassword")
    private WebElement confirmPassword;

    @FindBy(id = "register-button")
    private WebElement btnGoToRegister;

    @FindBy(className = "result")
    private WebElement lblRegisterSuccess;

    private static final int DEFAULT_TIMEOUT = 30;

    public PageRegister() {
        PageFactory.initElements(internalActionWeb.getDriver(), this);
    }

    public boolean register(InternalActionWeb internalActionWeb) throws WebActionsException {
        internalActionWeb.click(btnRegister, DEFAULT_TIMEOUT, true);
        return internalActionWeb.isVisible(verifyRegisterPage, DEFAULT_TIMEOUT, true);
    }

    public void fixingRegister(String firstNameFill, String lastNameFill, String Email, Company companyFill, String passwordFill, String confirmPasswordFill) throws WebActionsException {
        internalActionWeb.click(btnGenderFemale, DEFAULT_TIMEOUT, true);
        internalActionWeb.sendText(firstName, firstNameFill, DEFAULT_TIMEOUT, true);
        internalActionWeb.sendText(lastName, lastNameFill, DEFAULT_TIMEOUT,true);
        internalActionWeb.sendText(email, Email, DEFAULT_TIMEOUT, true);
        //internalActionWeb.sendText(numeroIdentidad, numeroIdentidadFill, DEFAULT_TIMEOUT, true);
        internalActionWeb.sendText(company, String.valueOf(companyFill), DEFAULT_TIMEOUT, true);
        internalActionWeb.sendText(password, passwordFill, DEFAULT_TIMEOUT, true);
        internalActionWeb.sendText(confirmPassword, confirmPasswordFill, DEFAULT_TIMEOUT, true);
        internalActionWeb.click(btnGoToRegister, DEFAULT_TIMEOUT, true);
    }

    public boolean registerSuccess(InternalActionWeb internalActionWeb) throws WebActionsException{
            return internalActionWeb.isVisible(lblRegisterSuccess, DEFAULT_TIMEOUT,true);
    }

}
