package co.com.client.noCommerceProject.test.stepdefinition;

import co.com.client.desktopProject.test.helpers.TestInfo;
import co.com.client.noCommerceProject.test.controllers.LogInController;
import co.com.client.noCommerceProject.test.controllers.RegisterController;
import co.com.client.noCommerceProject.test.helpers.ExtentReport;
import co.com.client.noCommerceProject.test.helpers.InternalActionWS;
import co.com.client.noCommerceProject.test.internalaction.InternalActionWeb;
import co.com.sofka.test.actions.Action;
import co.com.sofka.test.evidence.reports.Assert;
import co.com.sofka.test.evidence.reports.Report;
import co.com.sofka.test.exceptions.WebActionsException;
import co.com.sofka.test.utils.files.PropertiesFile;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;
import java.nio.file.Paths;

public class StepsDefinition {

    //Controllers
    private RegisterController registerController = new RegisterController();
    private LogInController logInController = new LogInController();

    public static InternalActionWeb internalActionWeb;
    public static InternalActionWS internalActionWS;

    //Instancias globales
    public static PropertiesFile globalProps = Action.getPropertiesFiles(); // archivo propiedades por defecto
    private static Path propertiesFolder = Paths.get("src/main/resources/properties");// archivo propiedades personalizado
    public static PropertiesFile customProps = new PropertiesFile("custom", propertiesFolder);
    public static TestInfo testInfo;


    @Before
    public void setUp(Scenario scenario) {

        Assert.Soft.init();

        testInfo = new TestInfo(scenario);

        final String PROJECT_NAME = globalProps.getFieldValue("projectWebNoCommerce.name")
                .replace(StringUtils.SPACE,"_");
        internalActionWeb = new co.com.client.noCommerceProject.test.internalaction.InternalActionWeb(PROJECT_NAME);
        internalActionWS = new InternalActionWS(PROJECT_NAME);

        //Se imprime en los logs un diferenciador cada vez que inicia un escenario.Debe ir después del internalAction
        // ya que solo después de su instanciación la evidencia existe
        printInitLogs(testInfo);
        /*
        En caso de contar con alguna interfaz de reportería se debe agregar al reporteador con una clase
         creada por nosotros con esta interfaz implementada. Actualmente el proyecto base no cuenta con
         ninguna interfaz de reportería
         */
        ExtentReport extentReport = new ExtentReport();
        Report.setReporter(extentReport);
    }

    @After
    public void tearDown() {
        Assert.Soft.finish();
        //Se imprime en los logs un diferenciador cada vez que inicia un escenario
        printEndingLogs(testInfo);

        /*
        Se realizan todas las operaciones de cierre necesario sin importar el resultado del test
        (ej: finalizar los drivers instanciados)
         */
        internalActionWeb.closeBrowser();
        internalActionWeb.clearDriver();

    }

    //-----------------------Scenario HU00202--------------------------------------------------------------------------

    @Given("que estoy en la pagina nopCommerce para registrarme")
    public void queEstoyEnLaPaginaNopCommerceParaRegistrarme() {
        String url = globalProps.getFieldValue("app.url.noCommerce");
        registerController.startApp(url, testInfo.getFeatureName());
    }

    @When("ingreso a la sección de registro")
    public void ingresoALaSecciónDeRegistro() throws WebActionsException {
        registerController.goaAndVerifyRegister();
    }

    @And("diligencio todos los campos")
    public void diligencioTodosLosCampos() throws WebActionsException {
        registerController.fillPersonalDetails();
    }

    @Then("me registro exitosamente")
    public void meRegistroExitosamente() throws WebActionsException {
        registerController.verifySuccessRegister();
    }


    //-------------------Scenario HU00301, HU00302---------------------------------------------------------------------

    @Given("que estoy en la página nopCommerce para loguearme")
    public void queEstoyEnLaPáginaNopCommerceParaLoguearme() {
        String url = globalProps.getFieldValue("app.url.noCommerce");
        logInController.startApp(url, testInfo.getFeatureName());
    }

    @When("igreso a Log in")
    public void igresoALogIn() throws WebActionsException {
        logInController.goAndVerifyLogIn();
    }

    @And("ingreso email y password válido")
    public void ingresoEmailYPasswordVálido() throws WebActionsException {
        String emailCustomer = "camila.gamboa@sofka.com.co";
        String passwordCustomer = "caggrp1009";
        logInController.fillLogIn(emailCustomer, passwordCustomer);
    }

    @Then("igresare existosamente a la plataforma y mostrará el mensaje Welcome to our store")
    public void igresareExistosamenteALaPlataformaYMostraráElMensajeWelcomeToOurStore() throws WebActionsException {
        logInController.verifySuccessLogIn();
    }

    //---------------------Scenario HU00304------------------------------------------------------------------------------


    @And("ingreso email y password invalido")
    public void ingresoEmailYPasswordInvalido() throws WebActionsException {
        logInController.logInInvalid();
    }

    @Then("al presionar ingresar el sistema me muestra un mensaje de error")
    public void alPresionarIngresarElSistemaMeMuestraUnMensajeDeError() throws WebActionsException {
        logInController.verifyMistakeMessage();
    }

    //---------------------Scenario HU00306

    @Given("que estoy logueado en la página nopCommerce")
    public void queEstoyLogueadoEnLaPáginaNopCommerce() throws WebActionsException {
        String url = globalProps.getFieldValue("app.url.noCommerce");
        logInController.startApp(url, testInfo.getFeatureName());
        logInController.goAndVerifyLogIn();
        String emailCustomer = "camila.gamboa@sofka.com.co";
        String passwordCustomer = "caggrp1009";
        logInController.fillLogIn(emailCustomer, passwordCustomer);
        logInController.verifySuccessLogIn();
    }

    @When("le doy click en log out para salir de la plataforma")
    public void leDoyClickEnLogOutParaSalirDeLaPlataforma() throws WebActionsException {
        logInController.logOutOfPlatform();
    }

    @Then("regresaré a la pagina de inicio y estará disponible el botón de Log in")
    public void regresaréALaPaginaDeInicioYEstaráDisponibleElBotónDeLogIn() throws WebActionsException {
        logInController.goAndVerifyLogIn();
    }


    public void printInitLogs(TestInfo testInfo) {
        String initMsg = StringUtils.LF
                .concat("*****************************************************************************************************************************************************")
                .concat(StringUtils.LF)
                .concat("Inicio ").concat(testInfo.getFeatureName())
                .concat(StringUtils.LF)
                .concat(">> ESCENARIO: ").concat(testInfo.getScenarioValue())
                .concat(StringUtils.LF)
                .concat("*****************************************************************************************************************************************************");
        //Se imprime en los 3 logs para agrupar
        Report.Business.logInfo(initMsg);
        Report.TestLog.logInfo(initMsg);
        Report.Evidence.log(initMsg);
    }

    public void printEndingLogs(TestInfo testInfo) {
        String initMsg = StringUtils.LF
                .concat("*****************************************************************************************************************************************************")
                .concat(StringUtils.LF)
                .concat("FIN ").concat(testInfo.getFeatureName())
                .concat(StringUtils.LF)
                .concat(">> ESCENARIO: ").concat(testInfo.getScenarioValue())
                .concat(StringUtils.LF)
                .concat(">> RESULTADO: ").concat(testInfo.getStatus())
                .concat(StringUtils.LF)
                .concat("*****************************************************************************************************************************************************");
        //Se imprime en los 3 logs para agrupar
        Report.Business.logInfo(initMsg);
        Report.TestLog.logInfo(initMsg);
        Report.Evidence.log(initMsg);
    }


}
