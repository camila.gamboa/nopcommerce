package co.com.client.noCommerceProject.test.stepdefinition;

import co.com.client.desktopProject.test.helpers.TestInfo;
import co.com.client.noCommerceProject.test.controllers.ComputersController;
import co.com.client.noCommerceProject.test.helpers.ExtentReport;
import co.com.client.noCommerceProject.test.helpers.InternalActionWS;
import co.com.client.noCommerceProject.test.internalaction.InternalActionWeb;
import co.com.sofka.test.actions.Action;
import co.com.sofka.test.evidence.reports.Assert;
import co.com.sofka.test.evidence.reports.Report;
import co.com.sofka.test.exceptions.WebActionsException;
import co.com.sofka.test.utils.files.PropertiesFile;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;
import java.nio.file.Paths;

public class StepsDefinitionComputers {

    private ComputersController computersController = new ComputersController();

    public static InternalActionWeb internalActionWeb;
    public static InternalActionWS internalActionWS;

    //Instancias globales
    public static PropertiesFile globalProps = Action.getPropertiesFiles(); // archivo propiedades por defecto
    private static Path propertiesFolder = Paths.get("src/main/resources/properties");// archivo propiedades personalizado
    public static PropertiesFile customProps = new PropertiesFile("custom", propertiesFolder);
    public static TestInfo testInfo;

    @Before
    public void setUp(Scenario scenario) {

        Assert.Soft.init();

        testInfo = new TestInfo(scenario);

        final String PROJECT_NAME = globalProps.getFieldValue("projectWebNoCommerce.name")
                .replace(StringUtils.SPACE,"_");
        internalActionWeb = new co.com.client.noCommerceProject.test.internalaction.InternalActionWeb(PROJECT_NAME);
        internalActionWS = new InternalActionWS(PROJECT_NAME);

        printInitLogs(testInfo);

        ExtentReport extentReport = new ExtentReport();
        Report.setReporter(extentReport);
    }

    @After
    public void tearDown() {
        Assert.Soft.finish();
        printEndingLogs(testInfo);

        internalActionWeb.closeBrowser();
        internalActionWeb.clearDriver();

    }

    //--------------------------Scenario HU00106
    //--------------------------Caso de prueba 1

    @Given("que estoy en la pagina de computers")
    public void queEstoyEnLaPaginaDeComputers() throws WebActionsException {
        String url = globalProps.getFieldValue("app.url.noCommerce");
        computersController.startApp(url, testInfo.getFeatureName());
        computersController.gotoComputers();
    }

    @When("ingreso a la cateogria Desktops")
    public void ingresoALaCateogriaDesktops() throws WebActionsException {
        computersController.goToCategoryDesktop();
    }

    @And("los posiciono en lista")
    public void losPosicionoEnLista() throws WebActionsException {
        computersController.listForm();
    }

    @Then("mostrara la informacion de los productos")
    public void mostraraLaInformacionDeLosProductos() throws WebActionsException {
        computersController.infoProductsDesktop();
    }

    //--------------------------Caso de prueba 2

    @When("ingreso a la cateogria Notebooks")
    public void ingresoALaCateogriaNotebooks() throws WebActionsException {
        computersController.goToCategoryNotebooks();
    }

    @And("los posiciono en lista los Noteboooks")
    public void losPosicionoEnListaLosNoteboooks() throws WebActionsException {
        computersController.listForm();
    }

    @Then("mostrara la informacion de los productos de esta categoria")
    public void mostraraLaInformacionDeLosProductosDeEstaCategoria() throws WebActionsException {
        computersController.infoProductsNotebooks();
    }

    public void printInitLogs(TestInfo testInfo) {
        String initMsg = StringUtils.LF
                .concat("*****************************************************************************************************************************************************")
                .concat(StringUtils.LF)
                .concat("Inicio ").concat(testInfo.getFeatureName())
                .concat(StringUtils.LF)
                .concat(">> ESCENARIO: ").concat(testInfo.getScenarioValue())
                .concat(StringUtils.LF)
                .concat("*****************************************************************************************************************************************************");
        //Se imprime en los 3 logs para agrupar
        Report.Business.logInfo(initMsg);
        Report.TestLog.logInfo(initMsg);
        Report.Evidence.log(initMsg);
    }

    public void printEndingLogs(TestInfo testInfo) {
        String initMsg = StringUtils.LF
                .concat("*****************************************************************************************************************************************************")
                .concat(StringUtils.LF)
                .concat("FIN ").concat(testInfo.getFeatureName())
                .concat(StringUtils.LF)
                .concat(">> ESCENARIO: ").concat(testInfo.getScenarioValue())
                .concat(StringUtils.LF)
                .concat(">> RESULTADO: ").concat(testInfo.getStatus())
                .concat(StringUtils.LF)
                .concat("*****************************************************************************************************************************************************");
        //Se imprime en los 3 logs para agrupar
        Report.Business.logInfo(initMsg);
        Report.TestLog.logInfo(initMsg);
        Report.Evidence.log(initMsg);
    }



}
