package co.com.client.noCommerceProject.test.controllers;

import co.com.client.noCommerceProject.test.page.PageComputers;
import co.com.client.noCommerceProject.test.stepdefinition.StepsDefinition;
import co.com.client.noCommerceProject.test.stepdefinition.StepsDefinitionComputers;
import co.com.sofka.test.automationtools.selenium.Browser;
import co.com.sofka.test.evidence.reports.Assert;
import co.com.sofka.test.evidence.reports.Report;
import co.com.sofka.test.exceptions.WebActionsException;

public class ComputersController {



    private final String mensajeExitoso = "Se ha realizado exitosamente la accion.";
    private final String mensajeNoExitoso = "Ocurrió un error realizando la accion";

    public void startApp(String url, String feature) {
        final String accion = "Iniciación de plataforma";
        Browser browser = new Browser();
        browser.setBrowser(Browser.Browsers.CHROME);
        browser.setMaximized(true);
        browser.setIncognito(true);
        browser.setAutoDriverDownload(true);
        try {
            StepsDefinition.internalActionWeb.startWebApp(browser, url, feature);
            Report.reportScreenshot(StepsDefinition.internalActionWeb);
            Report.reportSuccess(String.format(mensajeExitoso, accion));
        } catch (WebActionsException e) {
            Report.reportScreenshot(StepsDefinition.internalActionWeb);
            Report.reportFailure(String.format(mensajeNoExitoso, accion),e);
        }
    }

    public void gotoComputers() throws WebActionsException {
        PageComputers pageComputers = new PageComputers();
        pageComputers.categorysOfComputers();
    }

    public void goToCategoryDesktop() throws WebActionsException {
        PageComputers pageComputers = new PageComputers();;
        pageComputers.btnCategoryDesktop();
    }
    public void listForm() throws WebActionsException {
        PageComputers pageComputers = new PageComputers();;
        pageComputers.setListForm();
    }
    public void infoProductsDesktop() throws WebActionsException {
        PageComputers pageComputers = new PageComputers();
        for(int i=0; i<=2; i++){

        }
        boolean product1 = pageComputers.nameOfProducts(StepsDefinitionComputers.internalActionWeb);
        Assert.Soft.thatIsEqual(product1, "Build your own computer", "Digital Storm VANQUISH 3 Custom Performance PC");
//        boolean product2 = pageComputers.nameOfProducts(StepsDefinitionComputers.internalActionWeb);
//        Assert.Soft.thatIsEqual(product2, "Digital Storm VANQUISH 3 Custom Performance PC");
//        boolean product3 = pageComputers.nameOfProducts(StepsDefinitionComputers.internalActionWeb);
//        Assert.Soft.thatIsEqual(product3, "Lenovo IdeaCentre 600 All-in-One PC");
//
    }
    public void goToCategoryNotebooks() throws WebActionsException {
        PageComputers pageComputers = new PageComputers();
        pageComputers.btnCategoryNotebooks();
    }
    public void infoProductsNotebooks() throws WebActionsException {
        PageComputers pageComputers = new PageComputers();;
        boolean product1 = pageComputers.nameOfProducts(StepsDefinitionComputers.internalActionWeb);
        Assert.Soft.thatIsEqual(product1, "Apple MacBook Pro 13-inch");
        boolean product2 = pageComputers.nameOfProducts(StepsDefinitionComputers.internalActionWeb);
        Assert.Soft.thatIsEqual(product2, "Asus N551JK-XO076H Laptop");
        boolean product3 = pageComputers.nameOfProducts(StepsDefinitionComputers.internalActionWeb);
        Assert.Soft.thatIsEqual(product3, "HP Envy 6-1180ca 15.6-Inch Sleekbook");
    }

}
