package co.com.client.noCommerceProject.test.page;

import co.com.client.noCommerceProject.test.internalaction.InternalActionWeb;
import co.com.client.noCommerceProject.test.stepdefinition.StepsDefinition;
import co.com.sofka.test.exceptions.WebActionsException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static co.com.client.noCommerceProject.test.stepdefinition.StepsDefinition.internalActionWeb;

public class PageLogIn {

    @FindBy(xpath = "//a[@class='ico-login']")
    private WebElement btnLogIn;

    @FindBy(xpath = "//input[@value='Register']")
    private WebElement btnRegister;

    @FindBy(id = "Email")
    private WebElement emailInput;

    @FindBy(id = "Password")
    private WebElement passwordInput;

    @FindBy(xpath = "//input[@class='button-1 login-button']")
    private WebElement btnGoToLogIn;

    @FindBy(xpath = "//h2[contains(text(),'Welcome to our store')]")
    private WebElement lblWelcomeToOurStore;

    @FindBy(xpath = "//div[@class='message-error validation-summary-errors']")
    private WebElement lblMistakeMessage;

    @FindBy(id = "Email-error")
    private WebElement wrongEmail;

    @FindBy(xpath = "//a[@class='ico-logout']")
    private WebElement btnLogOut;


    private static final int DEFAULT_TIMEOUT = 30;

    public PageLogIn() {
        PageFactory.initElements(internalActionWeb.getDriver(), this);
    }

    public boolean logInPage(InternalActionWeb internalActionWeb) throws WebActionsException {
        internalActionWeb.click(btnLogIn, DEFAULT_TIMEOUT, true);
        return internalActionWeb.isVisible(btnRegister, DEFAULT_TIMEOUT, true);
    }

    public void returningCustomer(String emailCustomer, String passwordCustomer) throws WebActionsException {
        internalActionWeb.sendText(emailInput, emailCustomer, DEFAULT_TIMEOUT, true);
        internalActionWeb.sendText(passwordInput, passwordCustomer, DEFAULT_TIMEOUT, true);
    }

    public boolean verifyLogIn(InternalActionWeb internalActionWeb) throws WebActionsException {
        internalActionWeb.click(btnGoToLogIn, DEFAULT_TIMEOUT, true);
        return internalActionWeb.isVisible(lblWelcomeToOurStore, DEFAULT_TIMEOUT, true);
    }

    public boolean invalidLogInMessage(InternalActionWeb internalActionWeb) throws WebActionsException {
        internalActionWeb.click(btnGoToLogIn, DEFAULT_TIMEOUT, true);
        return internalActionWeb.isVisible(lblMistakeMessage, DEFAULT_TIMEOUT, true);
    }

    public void logOutPlatform(InternalActionWeb internalActionWeb) throws WebActionsException {
        StepsDefinition.internalActionWeb.click(btnLogOut, DEFAULT_TIMEOUT, true);
    }





}
