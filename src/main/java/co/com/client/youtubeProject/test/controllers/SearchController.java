package co.com.client.youtubeProject.test.controllers;
import static org.junit.Assert.*;

import co.com.client.youtubeProject.test.internalaction.InternalActionWeb;
import co.com.client.youtubeProject.test.page.PageHome;
import co.com.client.youtubeProject.test.stepsdefinition.StepsDefinition;
import co.com.sofka.test.automationtools.selenium.Browser;
import co.com.sofka.test.evidence.reports.Assert;
import co.com.sofka.test.evidence.reports.Report;
import co.com.sofka.test.exceptions.WebActionsException;

public class SearchController {
    private final String MSG_SUCCESSYOUTUBE = "Se ha realizado exitosamente %s en YouTube.";
    private final String MSG_ERRORYOUTUBE = "Ocurrió un error realizando %s  en YouTube.";
    private InternalActionWeb internalActionWeb;
    private PageHome pageHome;

    public SearchController(InternalActionWeb internalActionWeb) {
        this.internalActionWeb = internalActionWeb;
    }

    public void iniciarAplicacionYoutube(String urlYouTube, String feature, String scenario) {
        final String ACCION = "INICIACION YOUTUBE";
        String validScenario = scenario.replace(" ", "_");
        String validFeature = feature.replace(" ", "_");
        Browser browser = new Browser();
        browser.setBrowser(Browser.Browsers.CHROME);
        browser.setMaximized(true);
        browser.setIncognito(true);
        browser.setDriverVersion("72.0.3626.69");
        browser.setAutoDriverDownload(true);

        try {
            StepsDefinition.internalActionWeb.startWebApp(browser, urlYouTube, validFeature, validScenario);
            Report.reportScreenshot(StepsDefinition.internalActionWeb);
            Report.reportSuccess(String.format(MSG_SUCCESSYOUTUBE, ACCION));
        } catch (WebActionsException e) {
            Report.reportScreenshot(StepsDefinition.internalActionWeb);
            Report.reportFailure(String.format(MSG_ERRORYOUTUBE, ACCION), e);
        }
    }

    public void searchSong(String song) throws WebActionsException {
        final String ACCION = "ELECCIÓN DE CANCION";
        try {
            PageHome searchSongPage = new PageHome(internalActionWeb);
            searchSongPage.textSongSearched(song);
            System.out.println(song);
            Assert.Hard.thatIsEqual("macarena", "lalala");
            Report.reportScreenshot(co.com.client.webProject.test.stepdefinition.StepsDefinition.internalActionWeb);
            Report.reportSuccess(String.format(MSG_SUCCESSYOUTUBE, ACCION));
        } catch (WebActionsException e) {
            Report.reportScreenshot(co.com.client.webProject.test.stepdefinition.StepsDefinition.internalActionWeb);
            Report.reportSuccess(String.format(MSG_SUCCESSYOUTUBE, ACCION));
        }
    }
}
