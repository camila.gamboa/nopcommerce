package co.com.client.youtubeProject.test.stepsdefinition;

import co.com.client.desktopProject.test.helpers.TestInfo;
import co.com.client.youtubeProject.test.controllers.SearchController;
import co.com.client.youtubeProject.test.helpers.ExtentReport;
import co.com.client.youtubeProject.test.internalaction.InternalActionWS;
import co.com.client.youtubeProject.test.internalaction.InternalActionWeb;
import co.com.sofka.test.actions.Action;
import co.com.sofka.test.evidence.reports.Assert;
import co.com.sofka.test.evidence.reports.Report;
import co.com.sofka.test.exceptions.WebActionsException;
import co.com.sofka.test.utils.files.PropertiesFile;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;
import java.nio.file.Paths;

public class StepsDefinition {


    public static InternalActionWeb internalActionWeb;
    public static InternalActionWS internalActionWS;
    private SearchController searchControllerYouTube = new SearchController(internalActionWeb);


    // Utilidades Globales
    public static PropertiesFile globalPropsYouTube = Action.getPropertiesFiles(); // archivo propiedades por defecto
    private static Path propertiesFolderYouTube = Paths.get("src/main/resources/properties");// archivo propiedades personalizado
    public static PropertiesFile customPropsYouTube = new PropertiesFile("custom", propertiesFolderYouTube);
    public static TestInfo testInfoYouTube;


    @Before
    public void setUp(Scenario scenario) {

        Assert.Soft.init();


        testInfoYouTube = new TestInfo(scenario);

        final String PROJECT_NAME_YOUTUBE = globalPropsYouTube.getFieldValue("projectWebYouTube.name")
                .replace(StringUtils.SPACE, "_");
        internalActionWeb = new InternalActionWeb(PROJECT_NAME_YOUTUBE);
        internalActionWS = new InternalActionWS(PROJECT_NAME_YOUTUBE);

        printInitLogsYouTube(testInfoYouTube);

        ExtentReport extentReportYouTube = new ExtentReport();
        Report.setReporter(extentReportYouTube);
        searchControllerYouTube = new SearchController(internalActionWeb);
    }

    @After
    public void tearDown() {
        Assert.Soft.finish();

        printEndingLogs(testInfoYouTube);

        internalActionWeb.closeBrowser();
        internalActionWeb.clearDriver();
    }


    @Given("^that I am in the page of YouTube$")
    public void thatIAmInThePageOfYouTube() throws Throwable {
        String urlYouTube = globalPropsYouTube.getFieldValue("app.url.youtube");
        searchControllerYouTube.iniciarAplicacionYoutube(urlYouTube, testInfoYouTube.getFeatureName(), testInfoYouTube.getScenarioName());
    }

    @When("^I search the video (.+)$")
    public void iSearchTheVideo(String song) throws Throwable {
        searchControllerYouTube.searchSong(song);
    }

    @Then("^will appear the list of related videos$")
    public void willAppearTheListOfRelatedVideos() throws Throwable {

    }


    /*

    @Given("that I am in the page of YouTube")
    public void thatIAmInThePageOfYouTube() {
        String urlYouTube = globalPropsYouTube.getFieldValue("app.url.youtube");
        searchControllerYouTube.iniciarAplicacionYoutube(urlYouTube, testInfoYouTube.getFeatureName(), testInfoYouTube.getScenarioName());
    }

    @When("I search the video <nameVideo>")
    public void iSearchTheVideoNameVideo(String song) throws WebActionsException {
        searchControllerYouTube.searchSong(song);
    }

    @Then("will appear the list of related videos")
    public void willAppearTheListOfRelatedVideos() {
    }


 */
    public void printInitLogsYouTube(TestInfo testInfoYouTube) {
        String initMsg = StringUtils.LF
                .concat("*****************************************************************************************************************************************************")
                .concat(StringUtils.LF)
                .concat("INICIO BÚSQUEDA: ").concat(testInfoYouTube.getFeatureName())
                .concat(StringUtils.LF)
                .concat(">> ESCENARIO: ").concat(testInfoYouTube.getScenarioValue())
                .concat(StringUtils.LF)
                .concat("*****************************************************************************************************************************************************");
        //Se imprime en los 3 logs para agrupar
        Report.Business.logInfo(initMsg);
        Report.TestLog.logInfo(initMsg);
        Report.Evidence.log(initMsg);
    }

    public void printEndingLogs(TestInfo testInfoYouTube) {
        String initMsg = StringUtils.LF
                .concat("*****************************************************************************************************************************************************")
                .concat(StringUtils.LF)
                .concat("FIN BUSQUEDA: ").concat(testInfoYouTube.getFeatureName())
                .concat(StringUtils.LF)
                .concat(">> ESCENARIO: ").concat(testInfoYouTube.getScenarioValue())
                .concat(StringUtils.LF)
                .concat(">> RESULTADO: ").concat(testInfoYouTube.getStatus())
                .concat(StringUtils.LF)
                .concat("*****************************************************************************************************************************************************");
        //Se imprime en los 3 logs para agrupar
        Report.Business.logInfo(initMsg);
        Report.TestLog.logInfo(initMsg);
        Report.Evidence.log(initMsg);
    }
}
