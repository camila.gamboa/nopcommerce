package co.com.client.youtubeProject.test.page;

import co.com.client.youtubeProject.test.internalaction.InternalActionWeb;
import co.com.client.youtubeProject.test.stepsdefinition.StepsDefinition;
import co.com.sofka.test.exceptions.WebActionsException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static co.com.client.youtubeProject.test.stepsdefinition.StepsDefinition.internalActionWeb;


public class PageHome {

    private InternalActionWeb internalActionWeb;

    @FindBy(id = "search")
    private WebElement searchSong;

    @FindBy(id="search-icon-legacy")
    private WebElement btnSearch;

    private static final int DEFAULT_TIMEOUT = 30;

    public PageHome(InternalActionWeb internalActionWeb) {
        this.internalActionWeb = internalActionWeb;
        PageFactory.initElements(internalActionWeb.getDriver(), this);
    }

    public void textSongSearched(String song) throws WebActionsException {

        StepsDefinition.internalActionWeb.sendText(searchSong, song, DEFAULT_TIMEOUT,true);
        StepsDefinition.internalActionWeb.click(btnSearch, DEFAULT_TIMEOUT, true);
    }

}
