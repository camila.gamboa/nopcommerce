package co.com.client.webProject.test.page;

import co.com.client.webProject.test.stepdefinition.StepsDefinition;
import co.com.sofka.test.exceptions.WebActionsException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageIndex {

    @FindBy(className = "login")
    private WebElement lnkSingIn;

    @FindBy(id = "search_query_top")
    private WebElement txtSearch;

    @FindBy(name = "submit_search")
    private WebElement btnSubmitSearch;

    private static final int DEFAULT_TIMEOUT = 30;

    public PageIndex() {
        PageFactory.initElements(StepsDefinition.internalActionWeb.getDriver(), this);
    }

    public void ingresoAutenticacion() throws WebActionsException {
        StepsDefinition.internalActionWeb.click(lnkSingIn,DEFAULT_TIMEOUT, true);
    }

    public void ingresoProductoPorBuscador(String producto) throws WebActionsException {
        StepsDefinition.internalActionWeb.sendText(txtSearch, producto, DEFAULT_TIMEOUT,true);
        StepsDefinition.internalActionWeb.click(btnSubmitSearch,DEFAULT_TIMEOUT, true);
    }
}
