package co.com.client.webProject.test.page;

import co.com.client.webProject.test.internalaction.InternalActionWeb;

import co.com.client.webProject.test.stepdefinition.StepsDefinition;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageMyAccount {

    @FindBy(className = "page-heading")
    private WebElement lblMyAccount;

    private static final int DEFAULT_TIMEOUT = 30;

    public PageMyAccount() {
        PageFactory.initElements(StepsDefinition.internalActionWeb.getDriver(), this);
    }

    public boolean verifyElementMyAccount(InternalActionWeb internalAction) {
        return internalAction.isVisible(lblMyAccount, DEFAULT_TIMEOUT, true);
    }
}
